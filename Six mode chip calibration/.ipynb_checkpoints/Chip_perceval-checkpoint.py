import perceval as pcvl
import perceval.components as comp
import numpy as np
from gekko import GEKKO

def alpha_generator(hmodes, dl=20,ul=30,c=0.2):
    '''
    hmodes: list of int, location of the TOPS along the waveguide column.
    dl: float, lower bound of the 0-distance cross-talk coefficient.
    ul: float, upper bound of the 0-distance cross-talk coefficient.
    c: float, ratio between two adjacent cross-talk coefficients.
    '''
    alpha = np.array([[np.random.uniform(dl,ul)*(2*np.sum(i <= j)-1)*c**np.abs(i-j) for i in range(len(hmodes))] for j in range(len(hmodes))])
    print(alpha)
    return alpha

def gekko_solver(nmodes, hmodes, alpha, shift, factor, factor2):
    '''
    nmodes: int, number of circuit modes.
    hmodes: list of int, location of the TOPS along the waveguide column.
    alpha: list of float, internal or external alpha matrix.
    shift: int, shift of the TOPS along a column.
    factor: int, scale factor for the generation of the gamma coefficient.
    factor2: int, scale factor for the generation of the gamma coefficient.
    '''
    m = GEKKO()
    m.options.REDUCE=3
    gamma = np.roll([40*(0.2**i) for i in range(nmodes)], hmodes[0])
    x = m.Array(m.Var,nmodes,value=0,lb=0)
    alfa_vector = np.roll(alpha,-shift)
    a = np.sum(hmodes[0]==0)
    b = np.sum(hmodes[0]==1)
    
    m.Equation(x[hmodes[0]]*gamma[hmodes[0]] > x[-1+hmodes[0]]*gamma[-1+hmodes[0]])
    if shift != 0 or b==1:
         m.Equation(x[hmodes[0]-1]*gamma[hmodes[0]-1] > factor2*x[hmodes[0]+1]*gamma[hmodes[0]+1])
    m.Equation(x[hmodes[0]]*gamma[hmodes[0]] < 0.50)
    m.Equation(x[hmodes[0]]*gamma[hmodes[0]] > 0.35)
    m.Equation([x[(hmodes[i]+2*np.sum(i>=int(nmodes*0.5)-1-shift)*b)%nmodes]*gamma[(hmodes[i]+2*np.sum(i>=int(nmodes*0.5)-1-shift)*b)%nmodes] -
                x[(hmodes[i]+1+ 2*np.sum(i>=int(nmodes*0.5)-1-shift)*b)%nmodes]*gamma[(hmodes[i]+1+ 2*np.sum(i>=int(nmodes*0.5)-1-shift)*b)%nmodes] 
                  == alfa_vector[i] for i in range(len(hmodes))])
    
    m.Equation([x[(hmodes[i]+1+np.sum(i>int(nmodes*0.5)-1-shift-b))%nmodes]*gamma[(hmodes[i]+1+np.sum(i>int(nmodes*0.5)-1-shift-b))%nmodes] > 
                factor*x[(hmodes[i]+2-np.sum(i>int(nmodes*0.5)-1-shift-b))%nmodes]*gamma[(hmodes[i]+2-np.sum(i>int(nmodes*0.5)-1-shift-b))%nmodes] 
                for i in range(int(nmodes*0.5)-1)])
    
    
    
    m.solve(disp=False) # Solve
    
    gamma = [G*X for  G, X in zip(gamma, x)]
    return  np.roll(gamma, 2*shift)

def set_heaterresponseGEKKO(hmodes: list, nmodes=4, 
                            ctalks=[20,30,0.2], factor=2, factor2 = 0.35):
    '''
    hmodes: list of int, location of the TOPS along the waveguide column.
    nmodes: int, number of circuit modes.
    ctalks: list of float, lower bound, upper bound of the 0-distance cross-talk coefficient and ratio between two adjacent cross-talk coefficients.
    factor: int, scale factor for the generation of the gamma coefficient.
    factor2: int, scale factor for the generation of the gamma coefficient.
    '''
    dl, ul, c = ctalks
    if nmodes == 8:
        factor=1.1
        factor2 = 0.65
    alpha = alpha_generator(hmodes, dl=dl,ul=ul,c=c)
    tuple_gamma = tuple(gekko_solver(nmodes, hmodes, alpha[:,i],i, factor, factor2) for i in range(len(hmodes)))
    gamma = np.hstack(tuple_gamma)
    return gamma


def set_heaterresponse(hmodes: list, nmodes=4,
                       ctalks=[35,45,0.2]):
    '''
    hmodes: list of int, location of the TOPS along the waveguide column.
    nmodes: int, number of circuit modes.
    ctalks: list of float, lower bound, upper bound of the 0-distance cross-talk coefficient and ratio between two adjacent cross-talk coefficients.
    '''
    dl, ul, c = ctalks
    rngphase = lambda pos: np.roll(np.random.uniform(dl,ul,1)*[np.random.normal(c,c/10)**np.abs(i-nmodes) for i in range(nmodes*2 - 1)],pos-nmodes)[:nmodes]
    phasecontrol = np.array([rngphase(pos) for pos in hmodes]).T
    return phasecontrol


def alpha_construnction(nmodes, bm, disp):
    '''
    nmodes: int, number of circuit modes.
    bm: list of float, alpha block matrix for single column.
    disp: list of int, dispatch.
    '''
    
    vblock=tuple(np.roll(np.vstack((bm[i],np.zeros((int(nmodes*(nmodes-1)*0.5)-bm[i].shape[1],bm[i].shape[0]),dtype=np.int8))),np.roll(disp%int(nmodes*(nmodes-1)*0.5),1)[i],axis=0) for i in range(nmodes))
    alpha = np.hstack(vblock)
    return alpha

class Universal_Chip(pcvl.Circuit):
    '''
    Universal Multiport Interferometer (UMI), with thermal cross-talk, 
    class for simulation perfomed in perceval.
    -----------------------------------------
    nmodes: int, number of circuit modes.
    phi0: list of float, phase offset on the waveguides.
    ctalks: list of float, coefficients of thermal crosstalk for the generation of the gamma matrices.
    ctalks_method: str, method fo the generation of the gamma matrices.
    '''
    
    def __init__(self, nmodes = 4, phi0=None,
                 ctalks=[40,40,0.38], ctalks_method = 'random_gamma'):
        super().__init__(nmodes, name=f'UCHIP_{nmodes}modes')
        self.nmodes = nmodes
    
        for i in range(nmodes):
            hmodes = list(range(i%2,nmodes - i % 2,2)) 
            
            self.add(0,PS_column(nmodes, phi0=phi0, hmodes=hmodes, column=2*i, ctalks=ctalks, ctalks_method = ctalks_method))
            self.add(0,RC_column(nmodes, phi0=phi0, hmodes=hmodes, column=2*i+1, ctalks=ctalks, ctalks_method = ctalks_method))
            
        
        self._nheaters = int(nmodes*(nmodes-1)/2)
        self._dispatch = np.cumsum(np.array([nmodes/2,nmodes/2 - 1]*int(nmodes/2),dtype=int))
        
        self.nh = self._nheaters
        
            
    def set_BSpowers(self, power):
        '''
        power: list of int, power dissipated by the ITOPS.
        '''
        assert len(power) == self._nheaters
        
        to_dispatch = [x.reshape(len(x),1) for x in np.split(power,self._dispatch[:-1])]
        
        for i, pwr in enumerate(to_dispatch):
            self[0,2*i+1].powers = pwr
    
    def set_PSpowers(self, power):
        '''
        power: list of int, power dissipated by the ETOPS.
        '''
        assert len(power) == self._nheaters
        
        to_dispatch = [x.reshape(len(x),1) for x in np.split(power,self._dispatch[:-1])]
        
        for i, pwr in enumerate(to_dispatch):
            self[0,2*i].powers = pwr
            
    def get_BSpowers(self):
        nested_list = [self[0,2*i + 1].powers.tolist() for i in range(self.nmodes)]
        flattened_list = [val for sublist in nested_list for subsublist in sublist for val in subsublist]
        return flattened_list
    
    def get_PSpowers(self):
        nested_list = [self[0,2*i].powers.tolist() for i in range(self.nmodes)]
        flattened_list = [val for sublist in nested_list for subsublist in sublist for val in subsublist]
        return flattened_list
    
    def get_BSalphamatrix(self):
        self.BSalphamatrix = alpha_construnction(self.nmodes, [self[0, 2*i + 1].get_alphamatrix() for i in range(self.nmodes)], self._dispatch)
        return self.BSalphamatrix
        
    def get_PSalphamatrix(self):
        self.PSalphamatrix = alpha_construnction(self.nmodes, [self[0, 2*i].get_alphamatrix() for i in range(self.nmodes)], self._dispatch)
        return self.PSalphamatrix
    
    def display_chip(self, recursive):
        return pcvl.pdisplay(self, recursive=recursive)
    
    def measure(self, prob_choice, n_shot):
        '''
        prob_choice: bool, if True, probabilities are sampled, otherwise sample counts.
        n_shot: number of injected photon for the computation of the sample counts.
        '''
        p = pcvl.Processor("Naive", self)
        p.with_input(self._input_state)
        sampler = pcvl.algorithm.Sampler(p)
    
        if prob_choice==True:
            prob = sampler.probs()
            out_prob = prob['results'][self._output_state]
        else:
            count = sampler.sample_count(n_shot)
            out_prob = count['results'][self._output_state] / n_shot

        return out_prob 
    
    @property 
    def input_channel(self):
        return self._i
    
    @input_channel.setter
    def input_channel(self, i):
        '''
        i: int, index of the input channel.
        '''
        self._i = i
        input_state = [0 for i in range(self.nmodes)]
        input_state[self._i]=1
        self._input_state=pcvl.BasicState(input_state)
        print('stato input impostato:',self._input_state)
        
    @property 
    def output_channel(self):
        return self._o
    
    @output_channel.setter
    def output_channel(self, o):
        '''
        o: int, index of the output channel.
        '''
        self._o = o
        output_state = [0 for i in range(self.nmodes)]
        output_state[self._o]=1
        self._output_state= pcvl.BasicState(output_state)
        print('stato output impostato:',self._output_state)

class RC_column(pcvl.Circuit):
    '''
    A simple wrapper for a Reconfigurable Column (RC) of beam splitter.
    -------------------------------------------
    nmodes: int, number of circuit modes.
    phi0: list of float, phase offset on the waveguides.
    column: int, index of the RC column.
    ctalks: list of float, coefficients of thermal crosstalk for the generation of the gamma matrices.
    ctalks_method: str, method fo the generation of the gamma matrices.
    name: str, if provided, sets a name for the pcvl draw functions.
    '''
    
    def __init__(self, nmodes = 4,
                 phi0 = None, hmodes = None,
                 column = 0, ctalks=[40,40,0.38], ctalks_method = 'random_gamma'):
        
        super().__init__(nmodes, name=f'RC_{column}')
        
        self._hmode = hmodes
        if hmodes is None:
            self._hmode = [2*i for i in range(int(nmodes/2))]
        
        shift = False if (((column+1)/2)+1)%2==0 else True # Displace BS on odd columns
        
        self.in_phases = PS_column(nmodes, phi0, 
                                   hmodes, column, 
                                   ctalks, ctalks_method)
        
        self.add(0,BS_column(nmodes,shift=shift),merge=True)
        self.add(0,self.in_phases,merge=True)
        self.add(0,BS_column(nmodes,shift=shift),merge=True)
    
    def get_alphamatrix(self):
        self.alphamatrix = self.in_phases.get_alphamatrix()
        return self.alphamatrix
    
    @property
    def _PM(self):
        return self.in_phases._PM
    
    @property
    def _phi0(self):
        return self.in_phases._phi0
    
    @property
    def phases(self):
        return self.in_phases._phases
    @phases.setter
    def phases(self, phases: np.ndarray):
        '''
        phases: ndarray of float, induced phases by the PS of the RC.
        '''
        self.in_phases.phases = phases
        
    @property
    def powers(self):
        return self.in_phases._powers
    @powers.setter
    def powers(self, powers):
        '''
        power: list of float, power dissipated by the ITOPS of the RC.
        '''
        self.in_phases.powers = powers
        
class PS_column(pcvl.Circuit):
    '''
    A simple wrapper for a Phase Shifter (PS) column with thermal cross-talk. 
    Also, dynamically add phaseshifters to the circuits, creating
    the associated parameter list.
    -------------------------------
    
    nmodes: int, number of modes.
    phi0: ndarray of float, whether to add a random offset phase to the mode.
    hmodes: list of ints, indices of the modes containing a ITOPS/ETOPS.
    column: int, index of the PS/RC column.
    name: str, if provided, sets a name for the pcvl draw functions.
    '''
    def __init__(self, nmodes = 4,
                 phi0 = None, hmodes = None,
                 column = 0, ctalks=[40,40,0.38], ctalks_method = 'random_gamma'):
        
        super().__init__(nmodes, name=f'PS_{column}')
        
        self._hmode = hmodes
        self._nmodes = nmodes
        if hmodes is None:
            self._hmode = [2*i for i in range(int(nmodes/2))]
        
        self._phi0 = phi0
        if phi0 is None:
            self._phi0 = np.c_['c',np.random.random(size=self._nmodes)*1.5]
        
        [self.add(i,comp.PS(pcvl.P(f'\phi{column}_{i}'))) for i in range(nmodes)]
        self.paramlist = self.get_parameters()
        
        self._nh = len(self._hmode)
        
        if ctalks_method == 'random_gamma':
            self._PM = set_heaterresponse(hmodes=self._hmode, nmodes=self._nmodes, ctalks=ctalks)
        elif ctalks_method == 'constrained_gamma':
            self._PM = set_heaterresponseGEKKO(hmodes=self._hmode, nmodes=self._nmodes, ctalks=ctalks)
        else:
            raise ValueError('valore errato per ctalks_method. due valori possibili: random_gamma, constrained_gamma.')
            
        
        self._phases = np.c_['c',[0]*self._nmodes]
        self.powers = np.c_['c',[0]*len(self._hmode)]
        
    def get_alphamatrix(self):
        self.alphamatrix = (self._PM - np.roll(self._PM,-1,axis=0))[self._hmode[0]:self._nmodes - self._hmode[0]:2,]
        return self.alphamatrix
    
    @property
    def phases(self):
        return self._phases
    @phases.setter
    def phases(self, phases: np.ndarray):
        '''
        phases: list of float, induced phases by the PS of the RC.
        '''
        
        assert phases.shape == (self._nmodes, 1)
        self.power_on = False
        
        print(f'Column {self.name}, is no more power controlled')
        
        self._phases = phases
        [self.paramlist[i].set_value(self._phases[i,0]) for i in range(self._nmodes)]
        
    @property
    def powers(self):
        return self._powers
    @powers.setter
    def powers(self, powers):
        '''
        powers: list of float, power dissipated by the ITOPS of the RC.
        '''
        self.power_on = True
        
        if type(powers) == list:
            powers = np.c_['c', powers]
        self._powers = powers
        self._phases = self._PM @ powers + self._phi0
        [self.paramlist[i].set_value(self._phases[i,0]) for i in range(self._nmodes)]

class BS_column(pcvl.Circuit):
    '''
    A simple wrapper for a column of directional couplers (DC).
    ------------------------------
    
    nmodes: int, number of modes.
    shift: bool, whether to shift the couplers one mode below.
    name: str, if provided, sets a name for the pcvl draw functions.
    '''
    def __init__(self, nmodes = 4, shift=False, name='BS'):
        super().__init__(nmodes, name=name)
        [self.add(i,comp.BS()) for i in range(int(shift),nmodes-1,2)]