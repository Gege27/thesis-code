import numpy as np
import time
import qontrol
from Resistor import Resistor
from qlab.devices.LeoniFiberSwitch import LeoniFiberSwitch
from qlab.devices.OP710 import OP710

class Universal_Chip_Measure():
    '''
    Universal Multiport Interferometer (UMI), with thermal cross-talk, 
    class for measurement collection.
    --------------
    modes: int, number of circuit modes.
    '''
    def __init__(self, nmodes = 6):
        self.nmodes = nmodes
        self._nmzi = int(nmodes*(nmodes-1)/2)
       
        
    def open_fiberswitch(self):
        self.FS = LeoniFiberSwitch(7, 8)
        
    def close_fiberswitch(self):
        self.FS.__del__()
        
    def open_optotest(self, bulk_mode):
        '''
        bulk_mode: bool, if True the measurements are performed in bulk mode.
        '''
        self.pm = OP710(0)
        self.pm.bulk_mode = bulk_mode
        self.pm.wavelength = 785
        
    def close_optotest(self):
        self.pm.close()
        
    def open_qontrol(self):
        self.q = qontrol.QXOutput(serial_port_name = 'COM3')
        self.q.i[:] = 0
        self.q.v[:] = 4
        self.q.i[:] = 0
        
        R_P = [16,17,29,15,14,28,13,27,12,26,10,11,25,9,8,24,7,23,6,22,4,5,21,3,2,20,1,19,0,18]

        self._RR = [0]* self._nmzi*2
        for i in range(self._nmzi*2):
            with np.load(f'../6modes_characterization_eug/caratterization_res/{i}.npz') as f:
                popt = f['popt']
            self._RR[i] = Resistor(popt[0],popt[1],i,R_P[i], R_P[i])
        rext=[0,1,2,6,7,10,11,12,16,17,20,21,22,26,27] 
        rint=[3,4,5,8,9,13,14,15,18,19,23,24,25,28,29] 
        
        self._internal_phase =  [self._RR[i] for i in rint]
        self._external_phase =  [self._RR[i] for i in rext]
        
    def close_qontrol(self):
         self.q.close()
        
    def set_BSpowers(self, power):
        '''
        power: list of int, power dissipated by the ITOPS.
        '''
        assert len(power) == self._nmzi
        assert  np.any(np.array(power)<=50) and np.any(np.array(power)>=0)
        
        for i, pwr in enumerate(power):
            self.q.i[self._internal_phase[i].N_ch] = float(self._internal_phase[i].P_I(pwr))
        
        
    
        
    def set_PSpowers(self, power):
        '''
        power: list of int, power dissipated by the ETOPS.
        '''
        assert len(power) == self._nmzi
        assert np.any(np.array(power)<=50) and np.any(np.array(power)>=0)
    
        for i, pwr in enumerate(power):
            self.q.i[self._external_phase[i].N_ch] = float(self._external_phase[i].P_I(pwr))
                
    def get_BSpowers(self):
        self._BSpowers= np.zeros(len(self._internal_phase))
        for i in range(len(self._internal_phase)):
            self._BSpowers[i] = float(self._internal_phase[i].I_P(self.q.i[self._internal_phase[i].N_ch]))
            
        return self._BSpowers
    
    def get_PSpowers(self):
        self._PSpowers= np.zeros(len(self._external_phase))
        for i in range(len(self._external_phase)):
            self._PSpowers[i] = float(self._external_phase[i].I_P( self.q.i[self._external_phase[i].N_ch]))
            
        return self._PSpowers
    
   
    #def measure(self, prob_choice=None, n_shot=None):
        #self.output_channel
         #return np.mean([self.pm.measure_all() for i in range(n_shot)],  axis=0)[self._o]
    
    def measure(self):
        #self.output_channel
        return np.mean(self.pm.mesure_bulk_int(),  axis=0)[self._o]
    
    @property 
    def input_channel(self):
        return self._i
    
    @input_channel.setter
    def input_channel(self, i):
        '''
        i: int, index of the input channel.
        '''
        a =[5,4,3,2,1,0]
        self._i = a[i] + 1
        self.FS.channel= self._i
        time.sleep(0.1)
        print('impostato canale di input numero:', i)
        
    @property 
    def output_channel(self):
        return self._o
    
    @output_channel.setter
    def output_channel(self, o):
        '''
        o: int, index of the output channel.
        '''
        a =[5,4,3,2,1,0]
        self._o = a[o]  
        print('impostato canale di output numero:', o)
        

