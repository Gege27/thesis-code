# Introduction

This git repository contains the python codes involved in the data collection and analysis of the experiments in the thesis project. The contents of the single folders are summarized in the following list:
1. **g2 computation**: estimation of the light degree of second-order coherence from the coincidence histogram.
2. **Example with perceval**: some methods of the perceval library are shown for analyzing a photonic circuit.
3. **Compute chip unitary**: estimation of intensity transfer matrix and unitary matrix.
4. **Six mode chip calibration**: classes for simulations/measurements and notebooks in which the measurement/simulation of the calibration of a 6-mode universal multiport interferometer is performed. 

